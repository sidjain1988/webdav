var myApp = angular.module('webdavApp', ['ngRoute', 'ngResource']);

myApp.config(

    function ($routeProvider) {
        
        $routeProvider
            .when('/', {
            templateUrl: 'login.html',
            controller: 'loginController'
        })
        .when('/loginError', {
            templateUrl: 'error.html',
            controller: 'errorController'
        })
        .when('/dir', {
            templateUrl: 'dir.html',
            controller: 'dirController'
        }) 
        .when('/dir/:file', {
            templateUrl: 'download.html',
            controller: 'downloadController'
        })
 
    });

myApp.controller('loginController', ['$scope', 'loginService', '$location', function ($scope, loginService, $location) {
    
    $scope.login = function() {
       var loginPromise = loginService.doLogin($scope.uname, $scope.pwd);
        
        loginPromise.then(function(isLoginSuccess){
            if(isLoginSuccess){
                $scope.errorMsg = '';
                $location.path('/dir');
            }
        else{
                $scope.errorMsg = 'Authentication Failed !!';
                $location.path('/loginError');
            }
        });
    };
    
}]);

myApp.controller('dirController',['$scope',function($scope){
    
}]);

myApp.controller('errorController',['$scope',function($scope){
    
}]);

myApp.service('loginService', function($resource,$q) {
    
    
    this.doLogin = function(uname, pwd){
        
        return $q(function(resolve, reject){
             let loginSuccess = false;
        $resource('http://localhost:8080/login/webapi/doLogin?username='+uname+'&password='+pwd)
        .get(function(result){
            resolve(result);
    } , function(){reject("Err1")});
        });
    };
});
/*
myApp.controller('weatherController', ['$scope', 'weatherService', '$resource', '$routeParams', function ($scope, weatherService, $resource, $routeParams) {

    
    $scope.cName = weatherService.cityName;
    $scope.days = $routeParams.days || '2';
   
    $scope.weatherResource = $resource("http://api.openweathermap.org/data/2.5/forecast/daily" ,{callback : "JSON_CALLBACK"}, {get : {method : "JSONP"} });
    
    $scope.weatherResult = $scope.weatherResource.get({APPID: '0d3055753be7640e6ea306701a250ecc' , q : $scope.cName, cnt : $scope.days})
    
    console.log($scope.weatherResult);
    
    $scope.convertToCelsius = function(tempInKelvin) {
        return Math.round(tempInKelvin - 273.15);
    };
    
    
}]);
*/

/*
myApp.directive('weatherResult', function() {
    return {
        restrict: 'E',
        templateUrl: 'result.html',
        scope: {
          format : '@dateFormat',
          weather : '=weatherObject',
          convertToCelsius : '&'
      }
    };
});*/

